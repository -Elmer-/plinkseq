#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include "char_tok.h"
#include <set>

int main( int argc , char ** argv ) 
{

  // take N matrices (as formatted for 'smp' input) and create a merged input matrix (where the "types" are now appended with the files)
  // importantly, each matrix must have the same # of permutations, but can feature different genes.
  // a value of 0 will be added for all absent tests (for original and all perms)

  const int np = argc - 1;
  if ( np == 0 || np % 2 ) { std::cerr << "expecting an even number of parameters (e,.g. mm file1 label1 file2 label2 )\n"; std::exit(1); }
  
  const int nm = np / 2;
  
  int nperm;
  std::map<std::string,std::vector<std::string> > file2tests;
  std::map<std::string,std::map<std::string,std::vector<std::vector<double> > > > gene2file2test2data;
  std::set<std::string> genes;

  // read through each matrix, and store in memory

  for (int f = 0 ; f < nm ; f++ ) 
    {

      std::cerr << "opening matrix " << argv[ 1 + f*2 ] << "\tas " << argv[ 2 + f*2 ] << "\n";
      
      std::string label = argv[ 2 + f * 2 ] ;

      std::ifstream F( argv[ 1 + f*2 ] , std::ios::in );
      if ( ! F.good() ) { std::cerr << "problem opening file\n"; exit(1); }

      std::string line;
      std::getline( F , line );
      if ( line == "" ) continue;
      int ncol;
      char_tok tok( line , &ncol , '\t' );
      if ( ncol < 2 )
	{
	  std::cerr << "problem with format of first line of file\n";
	  exit(1);
	}
      
      //
      // check num perms
      //
      
      int np = atoi( tok(0) );
      
      if ( f == 0 ) nperm = np;
      else 
	{
	  if ( np != nperm ) 
	    {
	      std::cerr << "can only combine matrices with similar numbers of permutations\n";
	      exit(1);
	    }
	}


      // store test names for this matrix
      int nt = tok.size() - 1;
      std::vector<std::string> d;	  
      for (int t=0;t<nt;t++) d.push_back( tok(t+1) );
      file2tests[ label ] = d;
      
      int ngene = 0;
      
      // now read each line
      while ( ! F.eof() )
	{
	  // assume correct # of rows
	  std::string gene;
	  F >> gene;
	  if ( gene == "" ) continue;
	  if ( F.eof() ) continue;

	  genes.insert(gene);

	  // ignores these, as 'dummies' for now
	  // these will not be carried over
	  // the actual p-value calculations do not use these in any case

	  std::string alta, altu, na, nu;
	  F >> alta >> altu >> na >> nu;
	  
	  std::vector<std::vector<double> > data( nt );
	  
	  for (int r=0;r<=nperm;r++) // nperm + 1 to include original data
	    for (int t=0;t<nt;t++)
	      {
		double x;
		F >> x;
		data[t].push_back( x );
	      }

	  // store
	  gene2file2test2data[ gene ][ label ] = data;
	  
	  
	  // next gene
	  ++ngene;
	}
      
      
      std::cerr << "read " << ngene << " genes from this file\n\n";

      F.close();

    }


  //
  // Output into a single merged file
  //

  std::cout << nperm ;
  
  std::set<std::string> allgene;
  
  std::map<std::string,std::vector<std::string> >::iterator ff = file2tests.begin();
  while ( ff != file2tests.end() )
    {
      std::vector<std::string> & tests = ff->second;
      for (int t=0;t<tests.size();t++)
	std::cout << "\t" << ff->first << "::" << tests[t] ;
      ++ff;
    }
  
  std::cout << "\n";

  // now each row/gene

  std::set<std::string>::iterator ii = genes.begin();
  while ( ii != genes.end() ) 
    {
      std::string gene = *ii;
      std::cout << gene << "\t0\t0\t1\t1"; // dummy C/C counts here

      int n = 0;
      
      std::map<std::string,std::vector<std::vector<double> > > & data = gene2file2test2data[ gene ];
      
      // note -- this is a horrible way to access the data, likely v. inefficient, 
      // but should only need to use this code sparingly

      // for each relpicate (and original)
      for (int k = 0 ; k <= nperm ; k++ )
	{
	  // for each matrix
	  
	  std::map<std::string,std::vector<std::string> >::iterator ff = file2tests.begin();
	  while ( ff != file2tests.end() )
	    {
	      
	      // did this file have this gene listed? 
	      std::map<std::string,std::vector<std::vector<double> > >::iterator jj = data.find( ff->first );

	      if ( jj == data.end() )
		{
		  int nt = ff->second.size();
		  for (int t = 0 ; t < nt; t++ )
		    std::cout << "\t0"; // null statistic		  
		}
	      else
		{
		  // for each test in that file
		  std::vector<std::vector<double> > & tests = jj->second;
		  for (int t = 0 ; t < tests.size(); t++ )
		    std::cout << "\t" << tests[t][k];	      
		}

	      ++ff;
	    }
	}
      
      // next gene
      std::cout << "\n";
      ++ii;
    }

  
  // std::set<std::string> genes;

  
  
  std::cerr << "all done\n";

  exit(0);
  
}
