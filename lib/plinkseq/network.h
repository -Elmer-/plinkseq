#ifndef __PSEQ_NET_H__
#define __PSEQ_NET_H__

#include <string>
#include <map>
#include <set>
#include <vector>
#include <ostream>
#include <cmath>

#include "plinkseq/crandom.h"

#include "sqlwrap.h"
#include "regions.h"
#include "matrix.h"

#include "plinkseq/netdb.h"
#include "plinkseq/net-connected-components.h"

/* Network is based on Nodes and Edges.
 * Nodes point to a Gene (should make permutation easier, just shuffling pointers).
 * Edges relate pairs of Nodes.
 */

#define DEFAULT_LOCUS_WEIGHT 1.0
#define DEFAULT_LOCUS_NAME ""

namespace NetFunc {
	class Gene;
	class Locus;
	class Edge;

	typedef std::set<const Gene*> ConstGeneSet;
	typedef std::map<const Gene*, double> GeneToDouble;

	typedef std::set<const Locus*> ConstLocusSet;
	typedef std::map<const Locus*, double> LocusToDouble;

	typedef std::map<std::string, std::string> KeyValMap;
	typedef std::map<std::string, KeyValMap> KeyKeyValMap;

	typedef std::set<std::string> StringSet;
	typedef std::list<std::string> StringList;

	typedef StringList NetDetailsData;
	typedef std::list<NetDetailsData> NetDetails;

	template<class Key, class Val>
	class IndexedKeyToVal {
	public:
		IndexedKeyToVal() {}
		~IndexedKeyToVal() {}

		const Val& operator[](const Key& key) const {
			return _keyToVal[key];
		}

		Val& operator[](const Key& key) {
			typename KeyToVal::iterator findIt = _keyToVal.find(key);
			if (findIt == _keyToVal.end()) {
				findIt = _keyToVal.insert(make_pair(key, Val())).first;
				_keys.push_back(key);
			}

			return findIt->second;
		}

		typedef std::map<Key, Val> KeyToVal;
		typename KeyToVal::const_iterator begin() const { return _keyToVal.begin(); }
		typename KeyToVal::const_iterator end()   const { return _keyToVal.end(); }

		typename KeyToVal::const_iterator find(const Key& key) const {
			return _keyToVal.find(key);
		}

		typedef std::list<Key> OrderedKeys;
		typename OrderedKeys::const_iterator keysBegin() const { return _keys.begin(); }
		typename OrderedKeys::const_iterator keysEnd() const { return _keys.end(); }

	private:
		KeyToVal _keyToVal;
		OrderedKeys _keys;
	};

	typedef IndexedKeyToVal<std::string, double> NameToVal;

	typedef std::pair<std::string, std::string> StringPair;
	typedef IndexedKeyToVal<StringPair, double> NamePairToVal;

	class NetStatsOutput {
	public:
		NetStatsOutput() : _summaryStats(new NameToVal()), _geneAndSeedStats(new NamePairToVal()) {}
		~NetStatsOutput() {
			delete _summaryStats;
			delete _geneAndSeedStats;
		}

		NameToVal* _summaryStats;
		NamePairToVal* _geneAndSeedStats;
	};

	typedef std::set<const Edge*> ConstEdgeSet;

	class Gene {
	public:
		Gene(const std::string& label)
		: _label(label), _weight(1.0) {
		}

		Gene(const Gene& g)
		: _label(g._label), _weight(g._weight) {
		}

		const std::string& getLabel() const { return _label; }

		const std::string toString() const { return getLabel(); }

		double getWeight() const { return _weight; }

	private:
		const std::string _label;
		const double _weight;
	};

	class Locus {
	public:
		Locus(const std::string& label, double weight) : _label(label), _weight(weight) {}

		const std::string& getLabel() const { return _label; }

		const std::string toString() const {
			std::string str = getLabel();

			if (str == "") // unnamed Locus, so use Gene strings
				str = getGenesString();

			if (str == "") // no Genes in Locus
				str = "<EMPTY LOCUS>";

			return str;
		}

		const std::string getGenesString(char sep = '+') const {
			std::set<std::string> genes;
			for (LocusGenes::const_iterator i = genesBegin(); i != genesEnd(); ++i)
				genes.insert((*i)->toString());

			std::stringstream strStream;
			for (std::set<std::string>::const_iterator i = genes.begin(); i != genes.end(); ++i) {
				if (i != genes.begin())
					strStream << sep;
				strStream << *i;
			}
			return strStream.str();
		}

		void addGene(const Gene* g) {
			_genes.insert(g);
		}

		unsigned int size() const {
			return _genes.size();
		}

		bool empty() const {
			return _genes.empty();
		}

		bool inLocus(const Gene* g) const {
			LocusGenes::const_iterator findG = _genes.find(g);
			return (findG != _genes.end());
		}

		double getWeight() const {
			return _weight;
		}

		typedef ConstGeneSet LocusGenes;
		LocusGenes::const_iterator genesBegin() const { return _genes.begin(); }
		LocusGenes::const_iterator genesEnd()   const { return _genes.end(); }

	private:
		std::string _label;
		double _weight;

		LocusGenes _genes;
	};

	class Group {
	public:
		Group(const std::string& label = "group")
		: _label(label), _memberLoci(new LocusList()) {
		}

		~Group() {
			delete _memberLoci;
		}

		const std::string& getLabel() const { return _label; }

		const Locus* addMember(const NetFunc::ConstGeneSet& genes, double weight = DEFAULT_LOCUS_WEIGHT, const std::string& locus = DEFAULT_LOCUS_NAME);

		unsigned int numSeeds() const {
			return _memberLoci->size();
		}

		unsigned int numSeedGenes() const {
			return _geneToLoci.size();
		}
		bool empty() const {
			return _memberLoci->empty();
		}

		void clear() {
			_memberLoci->clear();
			_nameToLocus.clear();
			_geneToLoci.clear();
		}

		const ConstLocusSet* getLoci(const Gene* g) const {
			const ConstLocusSet* loci = NULL;

			GeneToLoci::const_iterator findGene = _geneToLoci.find(g);
			if (findGene != _geneToLoci.end())
				loci = &(findGene->second);

			return loci;
		}

		bool inLoci(const Gene* g) const {
			return (getLoci(g) != NULL);
		}

		double getSumLociWeights(const Gene* g) const {
			double sum = 0.0;
			const ConstLocusSet* nodeLoci = this->getLoci(g);
			if (nodeLoci != NULL) {
				for (ConstLocusSet::const_iterator i = nodeLoci->begin(); i != nodeLoci->end(); ++i)
					sum += (*i)->getWeight();
			}
			return sum;
		}

		typedef std::list<Locus> LocusList;
		LocusList::const_iterator lociBegin() const { return _memberLoci->begin(); }
		LocusList::const_iterator lociEnd()   const { return _memberLoci->end(); }

		typedef std::map<const Gene*, ConstLocusSet> GeneToLoci;
		GeneToLoci::const_iterator genesBegin() const { return _geneToLoci.begin(); }
		GeneToLoci::const_iterator genesEnd()   const { return _geneToLoci.end(); }

	private:
		typedef std::map<std::string, Locus*> LocusMap;

		std::string _label;

		LocusList* _memberLoci;
		LocusMap _nameToLocus;

		GeneToLoci _geneToLoci;
	};

	class Edge;

	class Node {
	public:
		Node(Gene* g, int nodeInd) : _gene(g), _nodeInd(nodeInd) {}

		void setGene(Gene* g) { _gene = g; }
		const Gene* getGene() const { return _gene; }
		Gene* getGene() { return _gene; }

		int getNodeInd() const { return _nodeInd; }

		// primary representation of network (i.e. connection from this node to other nodes)
		typedef std::set<Edge> Neighbors;

		bool addEdgeTo(const Node* toNode, double w);
		bool hasEdgeTo(const Node* toNode) const;
		bool removeEdgeTo(const Node* toNode);

		inline unsigned int numNeighbors() const { return _neighbors.size(); }

		Neighbors::const_iterator neighbBegin() const { return _neighbors.begin(); }
		Neighbors::const_iterator neighbEnd() const { return _neighbors.end(); }

		void removeOutgoingEdges() { _neighbors.clear(); }

		bool operator<(const Node& rhs) const {
			return _nodeInd < rhs._nodeInd;
		}
		bool operator==(const Node& rhs) const {
			return _nodeInd == rhs._nodeInd;
		}
		bool operator!=(const Node& rhs) const {
			return !(*this == rhs);
		}

	private:
		Gene* _gene;
		int _nodeInd;

		Neighbors _neighbors;
	};

	class Edge {
	public:
		Edge(const Node* fromNode, const Node* toNode, double w = 1)
		: fromNode(fromNode), toNode(toNode), wgt(w) {
		}

		virtual ~Edge() {}

		bool operator<(const Edge& rhs) const {
			if (*fromNode != *rhs.fromNode)
				return *fromNode < *rhs.fromNode;
			if (*toNode != *rhs.toNode)
				return *toNode < *rhs.toNode;
			return false;
		}

		const Node* getFromNode() const { return fromNode; }
		const Node* getToNode()   const { return toNode; }
		double getWeight() const { return wgt; }

		virtual std::string toString() const {
			std::stringstream str;
			str << getFromNode()->getGene()->getLabel() << " <--> " << getToNode()->getGene()->getLabel();
			return str.str();
		}

	private:
		const Node* fromNode;
		const Node* toNode;
		double wgt; // weight from a to b
	};

	class IndirectEdge : public Edge {
	public:
		IndirectEdge(const Node* fromNode, const Node* toNode, double w = 1)
		: Edge(fromNode, toNode, w), _intermediates(ConstNodeSet()) {
		}

		virtual ~IndirectEdge() {}

		void addIntermediate(const Node* n) {
			_intermediates.insert(n);
		}

		virtual std::string toString() const {
			StringSet interGenes;
			for (ConstNodeSet::const_iterator i = _intermediates.begin(); i != _intermediates.end(); ++i)
				interGenes.insert((*i)->getGene()->getLabel());

			std::stringstream str;
			str << getFromNode()->getGene()->getLabel() << " <- {";
			for (StringSet::const_iterator i = interGenes.begin(); i != interGenes.end(); ++i) {
				if (i != interGenes.begin())
					str << ",";
				str << *i;
			}
			str << "} -> " << getToNode()->getGene()->getLabel();
			return str.str();
		}

	private:
		typedef std::set<const Node*> ConstNodeSet;
		ConstNodeSet _intermediates;
	};

	typedef std::pair<const Node*, const Node*> NodePair;
	typedef std::map<NodePair, Edge*> NodePairToEdge;
	typedef std::map<NodePair, IndirectEdge*> NodePairToIndirectEdge;

	template<class EdgeType>
	static EdgeType* ensureEdge(const Node* node1, const Node* node2, double wgt, std::map<NodePair, EdgeType*>* edgeMap) {
		if (edgeMap == NULL)
			return NULL;

		NodePair np(node1, node2);
		typename std::map<NodePair, EdgeType*>::const_iterator findIt = edgeMap->find(np);
		if (findIt == edgeMap->end()) {
			EdgeType* edg = new EdgeType(node1, node2, wgt);
			findIt = edgeMap->insert(make_pair(np, edg)).first;
		}
		return findIt->second;
	}

	template<class EdgeType>
	static void deleteEdgeMap(std::map<NodePair, EdgeType*>* edgeMap) {
		if (edgeMap == NULL)
			return;

		for (typename std::map<NodePair, EdgeType*>::const_iterator i = edgeMap->begin(); i != edgeMap->end(); ++i)
			delete i->second;
		delete edgeMap;
	}

	struct paths_t {
		int size() const {
			return paths.size();
		}
		std::vector<std::vector<Node*> > paths;
		std::vector<double> wgts;
	};

	// this is the actual in-memory representation of the network
	class Network {
		class DegreeStats;
		friend class DegreeStats;

	public:
		Network(NetDBase*, bool edge_weights, std::set<std::string>* excludes = NULL);

		Network(const Network& net);
		Network& operator=(const Network& net);

		~Network();

		int addNode(Gene* g);

		bool addUndirectedEdge(int ind1, int ind2, double w);
		bool addDirectedEdge(int fromInd, int toInd, double w);

		bool removeUndirectedEdge(int ind1, int ind2);
		bool removeDirectedEdge(int fromInd, int toInd);
		void removeAllEdges();

		bool hasUndirectedEdge(int ind1, int ind2) const { return hasDirectedEdge(ind1, ind2) && hasDirectedEdge(ind2, ind1); }
		bool hasDirectedEdge(int fromInd, int toInd) const { return _nodes[fromInd]->hasEdgeTo(_nodes[toInd]); }

		// Returns true iff checkIndex is in-bounds and it is currently a Node in this Net
		bool hasNode(int checkIndex) const { return checkIndex < numNodes(); }
		inline unsigned int numNodes() const { return _nodes.size(); }
		inline unsigned int numNeighbors(int index) const { if (!hasNode(index)) return 0; return _nodes[index]->numNeighbors(); }

		std::vector<std::string> node_labels() const;

		// degree of individual gene
		int degree(const std::string& g) const;

		Data::Matrix<double> Normalized_Laplacian() const;
		Data::Matrix<double> Laplacian() const;
		Data::Matrix<double> Adjacency() const;
		Data::Matrix<double> Influence_Matrix(const double gamma = 1) const;

		void basic_n(int* n_nodes, int* n_edges, int* n_nodes_in_network) const;

		// return mean and (optionally) whole degree-distribution (in 'dd')
		double degree_distribution(double* x0, std::map<int, int>* dd = NULL);

		// seeds
		const Group* getSeeds() const { return _seeds; }
		const Locus* addSeed(const std::list<std::string>& genes, double weight = DEFAULT_LOCUS_WEIGHT, const std::string& locus = DEFAULT_LOCUS_NAME);
		void clearSeeds();

		// optional second seed list
		const Group* getCseeds() const { return _cseeds; }
		const Locus* addCseed(const std::list<std::string>& genes, double weight = DEFAULT_LOCUS_WEIGHT, const std::string& locus = DEFAULT_LOCUS_NAME);
		void clearCseeds();

		void set_cseed_param(bool a, bool b) {
			if (a != _fix_cseed_genes_in_node_perms) {
				_fix_cseed_genes_in_node_perms = a;
				_degreeStats->clearDegrees();
			}

			_include_cseed_cseed_OR_seed_seed_connections_in_cross = b;
		}

		// use edge weights?
		void use_edge_weights(const bool b) {
			_use_edge_weights = b;
		}
		bool use_edge_weights() const {
			return _use_edge_weights;
		}

		void exclude_within_locus_edges(const bool b) {
			_exclude_within_locus_edges = b;
		}
		bool exclude_within_locus_edges() const {
			return _exclude_within_locus_edges;
		}

		enum CrossLocusEdges {
			sum,
			max,
			mean
		};

		void count_cross_locus_edges(const CrossLocusEdges cle) {
			_count_cross_locus_edges = cle;
		}
		CrossLocusEdges count_cross_locus_edges() const {
			return _count_cross_locus_edges;
		}

		/* set merge threshold for node degree-binning.
		 *
		 * Value --> meaning:
		 *
		 *  0     -->  complete merging (all degrees to one bin, i.e. no matching on degree)
		 *  1     -->  no binning
		 *  N > 2 -->  ensure degree-bins have at least N nodes
		 */
		void degree_merge_threshold(const int t) {
			if (t != _degree_merge_threshold) {
				_degree_merge_threshold = t;
				_degreeStats->clearDegrees();
			}
		}
		int degree_merge_threshold() const {
			return _degree_merge_threshold;
		}

		// use edge-permutation for unique degrees
		void edge_permutation(const bool b) {
			_use_edge_permutation = b;
		}
		bool edge_permutation() const {
			return _use_edge_permutation;
		}

		// using seed permutation (then, e.g., Net doesn't need to calculate genic scores)
		void seed_permutation(const bool b) {
			_use_seed_permutation = b;
		}
		bool seed_permutation() const {
			return _use_seed_permutation;
		}

		bool has_cseed_list() const {
			return !_cseeds->empty();
		}

		// options in net-stats (i.e., to speed up, can drop the calculation of some of these)
		void calc_common_interactors(const bool b) {
			_calc_common_interactors = b;
		}
		bool calc_common_interactors() const {
			return _calc_common_interactors;
		}

		void calc_genic_scores(const bool b) {
			_calc_genic_scores = b;
		}
		bool calc_genic_scores() const {
			return _calc_genic_scores;
		}

		void calc_general_connectivity(const bool b) {
			_calc_general_connectivity = b;
		}
		bool calc_general_connectivity() const {
			return _calc_general_connectivity;
		}

		void calc_indirect_connectivity(const bool b) {
			_calc_indirect_connectivity = b;
		}
		bool calc_indirect_connectivity() const {
			return _calc_indirect_connectivity;
		}

		// main netstats driver:
		NetStatsOutput* netstats_full(NetDetails* netDetails = NULL) const;

		template<class Key>
		static void update_statistics
		(const IndexedKeyToVal<Key, double>& stats, const IndexedKeyToVal<Key, double>& permd, IndexedKeyToVal<Key, double>* expected, IndexedKeyToVal<Key, double>* pvals) {
			typedef IndexedKeyToVal<Key, double> StatToVal;

			// Note -- unlike many cases, here if we do not observe a value for the
			// original statistic/gene in the permed set, we count that as a permd
			// value that does not exceed the original.

			const double EPS = 1e-12;
			for (typename StatToVal::KeyToVal::const_iterator ii = stats.begin(); ii != stats.end(); ++ii) {
				const Key& statKey = ii->first;
				const double obsStat = ii->second;

				typename StatToVal::KeyToVal::const_iterator ff = permd.find(statKey);
				if (ff != permd.end()) {
					const double permStat = ff->second;
					if (!Helper::realnum(permStat)) {
						std::stringstream str;
						str << "Value of " << permStat << " is not a real number";
						plog.warn(str.str());
					}

					if (permStat > obsStat) {
						++(*pvals)[statKey];
					}
					else if (fabs(permStat - obsStat) < EPS) {
						// for ties, a coin-toss
						if (CRandom::rand() < 0.5)
							++(*pvals)[statKey];
					}

					(*expected)[statKey] += permStat;
				}
				else {
					// ++(*pvals)[ statKey ];
				}
			}
		}

		// (direct) connections
		std::map<std::string, int> connections(const std::string& gene, const int depth = 1) const;

		// seed/bin stats
		typedef std::map<std::string, std::vector<double> > BinToStats;
		typedef std::map<std::string, std::string> BinToLabel;
		BinToStats seed_bin_stats(BinToLabel* labels);

		//
		// Graph algorithms
		//

		// shortest paths interface
		paths_t Dijkstra(Node* a) const;

		// connected components
		typedef DisjointSet<const Node*>::EquivSet EquivNodes;
		typedef DisjointSet<const Node*>::EquivSetList EquivNodesList;
		const EquivNodesList* calc_connected_components();

		typedef std::vector<std::set<std::string> > ConnComp;
		ConnComp get_connected_components_of_size(int s, int t = -1);

		int n_connected_components() {
			if (_components == NULL)
				calc_connected_components();
			return _components->size();
		}

		void clear_connected_components() {
			if (_components != NULL) {
				delete _components;
				_components = NULL;
			}
		}

		void permuteNodeGeneRelationships();

		// make a subnetwork, including only existing edges with weight >= delta:
		void create_subnetwork(double delta);

		// matrix -> network
		void create_network_from_matrix(const Data::Matrix<double>& m,
				const double thr, bool greater_than = true);

		// remove edges in existing network if they are below/above threshold
		void remove_edges_based_on_matrix_vals(const Data::Matrix<double>& m,
				const double thr, bool remove_less_than = true);

		void remove_edges_from_matrix_seed_score_enhanced(const Data::Matrix<double>& m, const double thr);

	private:
		// calc/store degree of each node
		DegreeStats* _degreeStats;

		// permute network
		bool _use_edge_permutation;
		bool _use_seed_permutation;

		// use edge weights from NETDB?
		bool _use_edge_weights;

		// count edges between genes in the same seed locus?
		bool _exclude_within_locus_edges;

		// How should multiple edges spanning the same pair of loci be handled?
		CrossLocusEdges _count_cross_locus_edges;

		// two-seed list mode?
		bool _fix_cseed_genes_in_node_perms;
		bool _include_cseed_cseed_OR_seed_seed_connections_in_cross;

		// see degree_merge_threshold() above for definition
		int _degree_merge_threshold;

		// options for net-statistics
		bool _calc_common_interactors;
		bool _calc_genic_scores;
		bool _calc_general_connectivity;
		bool _calc_indirect_connectivity;

		// 'human' label to internal 'Gene' representation
		typedef std::map<std::string, Gene*> LabelToGene;
		LabelToGene _label2gene;

		typedef std::list<Node*> NodeList;
		typedef std::list<const Node*> ConstNodeList;

		typedef std::vector<Node*> NodeVec;
		typedef std::vector<const Node*> ConstNodeVec;

		NodeVec _nodes;

		// primary store used to iterate over all genes, and do look-ups
		typedef std::map<Gene*, Node*> GeneToNode;
		GeneToNode _geneToNode;

		// connected _components
		EquivNodesList* _components;

		// seed genes
		Group* _seeds;

		// second group seed list (comparator seeds, _cseeds)
		Group* _cseeds;

		static const std::string DELTA_STATS_ARRAY[];
		static std::list<std::string> DELTA_STATS;

		/*
		 * FUNCTIONS
		 */
		void copyOtherNet(const Network& net);
		static Group* copyGroup(const Group* group, const std::map<const Gene*, const Gene*>& copyToNewGene);

		void deleteDataMembers();

		typedef std::map<int, NodeVec> DegreeToNodes;
		typedef std::map<int, std::string> DegreeToLabel;

		class DegreeStats {
		public:
			DegreeStats(const Network* net) : _net(net),
			_degree2node(), _merged_degree2node(), _merged_degree_labels() {}

			// Assumes that ds and this point to _net objects with the same number of Nodes:
			DegreeStats& operator=(const DegreeStats& ds);

			void clearDegrees() {
				_degree2node.clear();
				_merged_degree2node.clear();
				_merged_degree_labels.clear();
			}

			inline const DegreeToNodes& degree2node() { if (_degree2node.empty()) {updateDegrees();} return _degree2node; }
			inline const DegreeToNodes& merged_degree2node() { if (_merged_degree2node.empty()) {updateDegrees();} return _merged_degree2node; }
			inline const DegreeToLabel& merged_degree_labels() { if (_merged_degree_labels.empty()) {updateDegrees();} return _merged_degree_labels; }

			// this rebuilds the network w/ the current binning
			// For example, will assign cseeds to their own bin size 1 (i.e. do not permute), if _fix_cseed_node_perms == true
			void updateDegrees();

		private:
			const Network* _net;

			DegreeToNodes _degree2node;
			DegreeToNodes _merged_degree2node;
			DegreeToLabel _merged_degree_labels;

			// helper functions:
			void populate_degree2node();
			void merge_degree2node(const int th);
		};

		Gene* find_gene(const std::string& label) const {
			return _label2gene.find(label) == _label2gene.end() ? NULL : _label2gene.find(label)->second;
		}

		NetFunc::ConstGeneSet find_genes(const std::list<std::string>& labels) const;

		// find all connecting nodes up to depth N
		std::map<const Node*, int> partners(const Node* node, const int depth = 1) const;

		static void random_draw(std::vector<int>& a);

		typedef std::map<const Gene*, ConstGeneSet> GeneToGenes;

		typedef std::pair<const Locus*, const Locus*> LocPair;
		typedef std::map<LocPair, ConstEdgeSet> LocEdges;

		class BasicNetStats {
		public:
			BasicNetStats(const std::string& name) :
				_name(name), agg_seed_n_connections(0), agg_gene_n_connections(0) {}
			const std::string& name() const { return _name; }

			LocusToDouble seed_n_connections;
			double agg_seed_n_connections;
			GeneToDouble gene_n_connections;
			double agg_gene_n_connections;

			LocEdges seed_edges;
			ConstEdgeSet edges;
			ConstGeneSet genes;

		private:
			std::string _name;
		};

		class SingleSeedGroupStats {
		public:
			SingleSeedGroupStats() :
				n_seeds(0), n_seed_genes(0),
				general("general"), direct("direct"), indirect("indirect") {}

			double n_seeds;
			double n_seed_genes;

			// General connectivity
			BasicNetStats general;

			// Direct connectivity
			BasicNetStats direct;

			// Indirect connectivity
			BasicNetStats indirect;

			// Interactors mediating indirect connectivity
			GeneToDouble gene_n_interactor_count;
			GeneToGenes interactors;
		};

		class CrossSeedGroupStats {
		public:
			CrossSeedGroupStats() :
				self_cross("self_cross"), direct_cross("direct_cross"), indirect_cross("indirect_cross") {}

			// "Self" cross-connectivity [i.e., gene on both SEED and CSEED lists (meaningful in seed-permutation case, perhaps?)]
			BasicNetStats self_cross;

			// Direct cross-connectivity [i.e., SEED is directly connected to CSEED]
			BasicNetStats direct_cross;

			// Indirect cross-connectivity
			BasicNetStats indirect_cross;
		};

		/*
		 * StatisticAggregator: classes for generically aggregating statistic for pairs of Loci
		 */
		template<class Key>
		class StatisticAggregator {
		public:
			StatisticAggregator() {}
			virtual ~StatisticAggregator() {}

			Key addStat(const Key& key, const double val) {
				_internal_addStat(key, val);

				return key;
			}

			typedef std::map<Key, double> KeyToStat;

			virtual KeyToStat getKeyToStat() const = 0;

			static double getSumOfStats(const KeyToStat& kts) {
				double sum = 0.0;
				for (typename KeyToStat::const_iterator i = kts.begin(); i != kts.end(); ++i)
					sum += i->second;

				return sum;
			}

		protected:
			virtual void _internal_addStat(const Key& key, const double val) = 0;
		};

		template<class Key>
		class SumStatisticAggregator : public StatisticAggregator<Key> {
		public:
			SumStatisticAggregator() : StatisticAggregator<Key>(), _sumStats(new KeyToStat()) {}
			virtual ~SumStatisticAggregator() { delete _sumStats; }

		protected:
			typedef typename StatisticAggregator<Key>::KeyToStat KeyToStat;
			KeyToStat* _sumStats;

			virtual void _internal_addStat(const Key& key, const double val) {
				(*_sumStats)[key] += val;
			}

			virtual KeyToStat getKeyToStat() const { return *_sumStats; }
		};

		template<class Key>
		class MaxStatisticAggregator : public StatisticAggregator<Key> {
		public:
			MaxStatisticAggregator() : StatisticAggregator<Key>(), _maxStats(new KeyToStat()) {}
			virtual ~MaxStatisticAggregator() { delete _maxStats; }

		protected:
			typedef typename StatisticAggregator<Key>::KeyToStat KeyToStat;
			KeyToStat* _maxStats;

			virtual void _internal_addStat(const Key& key, const double val) {
				typename KeyToStat::iterator i = _maxStats->find(key);
				if (i == _maxStats->end())
					(*_maxStats)[key] = val;
				else if (val > i->second)
					i->second = val;
			}

			virtual KeyToStat getKeyToStat() const { return *_maxStats; }
		};

		template<class Key>
		class MeanStatisticAggregator : public StatisticAggregator<Key> {
		public:
			MeanStatisticAggregator() : StatisticAggregator<Key>(), _meanStats(new MeanStats()) {}
			virtual ~MeanStatisticAggregator() { delete _meanStats; }

		protected:
			typedef typename StatisticAggregator<Key>::KeyToStat KeyToStat;

			typedef std::pair<double, int> SumCount;
			typedef std::map<Key, SumCount> MeanStats;
			MeanStats* _meanStats;

			virtual void _internal_addStat(const Key& key, const double val) {
				typename MeanStats::iterator i = _meanStats->find(key);
				if (i == _meanStats->end())
					i = _meanStats->insert(make_pair(key, SumCount(0.0, 0))).first;

				SumCount& sc = i->second;
				sc.first += val;
				++(sc.second);
			}

			virtual KeyToStat getKeyToStat() const {
				KeyToStat kts;

				for (typename MeanStats::const_iterator i = _meanStats->begin(); i != _meanStats->end(); ++i) {
					const Key& key = i->first;
					const SumCount& sc = i->second;

					kts[key] = (sc.first / sc.second);
				}

				return kts;
			}
		};

		template<class Key>
		StatisticAggregator<Key>* getStatAggregator() const {
			CrossLocusEdges cle = count_cross_locus_edges();
			switch (cle) {
			case sum: {
				return new SumStatisticAggregator<Key>();
				break;
			}
			case max: {
				return new MaxStatisticAggregator<Key>();
				break;
			}
			case mean: {
				return new MeanStatisticAggregator<Key>();
				break;
			}
			default: {
				return NULL;
				break;
			}
			}
		}

		typedef StatisticAggregator<LocPair> LocPairStatisticAggregator;
		typedef StatisticAggregator<const Locus*> LocStatisticAggregator;

		template<class MapKey, class StatKey>
		StatisticAggregator<StatKey>*
		ensureAggregator(const MapKey& mapKey, std::map<MapKey, StatisticAggregator<StatKey>*>& statAggMap) const {
			typename std::map<MapKey, StatisticAggregator<StatKey>*>::iterator findMapKey = statAggMap.find(mapKey);
			if (findMapKey == statAggMap.end())
				findMapKey = statAggMap.insert(make_pair(mapKey, getStatAggregator<StatKey>())).first;
			return findMapKey->second;
		}

		template<class MapKey, class StatKey>
		static double sumAndDeleteAggregators(std::map<MapKey, StatisticAggregator<StatKey>*>& statAggMap, std::map<MapKey, double>& output) {
			double sum = 0.0;

			for (typename std::map<MapKey, StatisticAggregator<StatKey>*>::const_iterator mapKeyIt = statAggMap.begin(); mapKeyIt != statAggMap.end(); ++mapKeyIt) {
				const MapKey& mapKey = mapKeyIt->first;
				const StatisticAggregator<StatKey>* agg = mapKeyIt->second;
				double val = StatisticAggregator<StatKey>::getSumOfStats(agg->getKeyToStat());
				output[mapKey] = val;
				delete agg;
				sum += val;
			}
			statAggMap.clear();

			return sum;
		}

		typedef map<const Locus*, LocStatisticAggregator*> LocToLocStatsAggregator;
		typedef map<const Gene*, LocPairStatisticAggregator*> GeneToLocPairStatsAggregator;
		/*
		 * END: StatisticAggregator classes
		 */

		void updateNetStats(const Node* node1, const Node* node2, const Locus* loc1, const Locus* loc2, double wgt, LocToLocStatsAggregator& locEdgesAgg, GeneToLocPairStatsAggregator& geneEdgesAgg, Edge* edg = NULL, BasicNetStats* storeDetails = NULL) const;

		// helper function in netstats (does most of work for a given seed set):
		void netstats_singleSeedGroup(const Group* seeds, NetDetails* netDetails, SingleSeedGroupStats* groupStats) const;
		void addSingleSeedGroupStats(NetStatsOutput& stats, const SingleSeedGroupStats* groupStats, string groupPrefix) const;

		void netstats_crossSeedGroups(const Group* seeds1, const Group* seeds2, NetDetails* netDetails, CrossSeedGroupStats* crossStats) const;

		template<class Data>
		static void addDataVals(const std::map<const Data*, double>& data, const std::string& prefix, NamePairToVal& stats) {
			typedef std::map<const Data*, double> DataToDouble;
			for (typename DataToDouble::const_iterator i = data.begin(); i != data.end(); ++i) {
				const Data* d = i->first;
				const double val = i->second;
				stats[StringPair(prefix, d->toString())] = val;
			}
		}

		static void printEdges(const BasicNetStats& netStats, NetDetails* netDetails, list<const Group*>& seeds, const std::string& prefix, bool printABiffAlessThanB = false);

		// Basic network features

		// shortest path
		std::vector<Network::ConstNodeList> DijkstraGetShortestPaths(const Node* vertex,
				const std::vector<Network::ConstNodeVec>* previous) const;

		void DijkstraGetShortestPathsHelper(const Node* vertex, Network::ConstNodeList path,
				std::vector<Network::ConstNodeList>* paths,
				const std::vector<Network::ConstNodeVec>* previous) const;

		void DijkstraComputePaths(const Node* source,
				std::vector<double>& min_distance,
				std::vector<ConstNodeVec>& previous) const;
	};
}

#endif
