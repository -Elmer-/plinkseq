#ifndef __PSEQ_MAT_EXP_H__
#define __PSEQ_MAT_EXP_H__

#include <complex>

//
//  Complex functions.
//
std::complex <double> *c8mat_expm1 ( int n, std::complex <double> a[] );
//
//  Real functions.
//
double *r8mat_expm1 ( int n, double a[] );
double *r8mat_expm2 ( int n, double a[] );
double *r8mat_expm3 ( int n, double a[] );

#endif
