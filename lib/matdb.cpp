
#include "plinkseq/matdb.h"
#include "plinkseq/sqlz.h"
#include "plinkseq/matrix.pb.h"

// protoc lib/matrix.proto --cpp_out=.
// mv lib/matrix.pb.cc lib/matrix.pb.cpp
// mv lib/matrix.pb.h lib/plinkseq/

bool MatDBase::create( const std::string & name )
{
  if ( Helper::fileExists(name) ) return false;
  sql.open( name );
  sql.close();
  attach( name );
  index();
  return attached();
}

bool MatDBase::attach( const std::string & name )
{
  
  if ( name == "-" || name == "." ) { dettach(); return false; } 
  
  // auto-create
  if ( ! Helper::fileExists( name ) ) return create( name );
  
  sql.open( name );
  
  // register compression functions 
  
  // sqlite3_create_function( sql.pointer(), "mycompress", 1, SQLITE_UTF8, 0, &compressFunc, 0, 0);
  // sqlite3_create_function( sql.pointer(), "myuncompress", 1, SQLITE_UTF8, 0, &uncompressFunc, 0, 0);
  
  sql.synchronous(false);
  
  sql.query(" CREATE TABLE IF NOT EXISTS matrices("
            "   name       VARCHAR(20) NOT NULL, "
	    "   mat_id     INTEGER PRIMARY KEY, "
	    "   nrow       INTEGER NOT NULL, "
	    "   ncol       INTEGER NOT NULL, "
	    "   dtype      VARCHAR(20) , "
	    "   dclass     VARCHAR(20) , "	    
 	    " CONSTRAINT c1 UNIQUE(name) ); " );

  // blob == one column 

  sql.query(" CREATE TABLE IF NOT EXISTS columns("
            "   mat_id     INTEGER NOT NULL , "
	    "   col_id     INTEGER NOT NULL , " // 0..(ncol-1) column numbering in actual matrix
            "   data       BLOB NOT NULL ); " );
  
  init();
  
  index();
  
}


void MatDBase::index()
{
  sql.query( "CREATE INDEX IF NOT EXISTS ind1 ON matrices(name);");
  sql.query( "CREATE INDEX IF NOT EXISTS ind2 ON columns(mat_id,col_id);");
  // release();
  // init();
}

void MatDBase::drop_index()
{
  sql.query( "DROP INDEX IF EXISTS ind1;");
  sql.query( "DROP INDEX IF EXISTS ind2;");
}


bool MatDBase::init()
{    
  
  stmt_insert_matrix = sql.prepare( "INSERT OR REPLACE INTO matrices ( name , nrow, ncol , dclass ) values( :name , :nrow , :ncol , :dclass ) ; " );
  stmt_lookup_matrix = sql.prepare( "SELECT mat_id , nrow , ncol , dclass FROM matrices WHERE name == :name ; " );

  // no compression  
  stmt_insert_column = sql.prepare( "INSERT OR REPLACE INTO columns ( mat_id , col_id , data ) values( :mat_id , :col_id , :data ) ; " );
  stmt_lookup_column = sql.prepare( "SELECT col_id , data FROM columns WHERE mat_id == :mat_id ORDER BY col_id ; " );
  
  // using compression
  // stmt_insert_column = sql.prepare( "INSERT OR REPLACE INTO columns ( mat_id , col_id , data ) values( :mat_id , :col_id , mycompress( :data ) ) ; " );
  // stmt_lookup_column = sql.prepare( "SELECT col_id , myuncompress( data ) FROM columns WHERE mat_id == :mat_id ORDER BY col_id ; " );

  stmt_dump_matrix_names = sql.prepare( "SELECT name , nrow , ncol , dclass FROM matrices ORDER BY name ; " );
}


bool MatDBase::release()
{    
  sql.finalise( stmt_insert_matrix );
  sql.finalise( stmt_lookup_matrix );
  sql.finalise( stmt_insert_column );
  sql.finalise( stmt_lookup_column );
  sql.finalise( stmt_dump_matrix_names );
}


bool MatDBase::dettach()
{
  if ( attached() ) 
    {
      release();
      sql.close();  
    }
}


std::vector<std::string> MatDBase::names() 
{
  std::vector<std::string> r;
  while ( sql.step( stmt_dump_matrix_names ) )
    {
      std::string n = sql.get_text( stmt_dump_matrix_names , 0 );
      int nrow = sql.get_int( stmt_dump_matrix_names , 1 );
      int ncol = sql.get_int( stmt_dump_matrix_names , 2 );
      std::string dclass = sql.get_text( stmt_dump_matrix_names , 3 );
      r.push_back( n + " (" + Helper::int2str( nrow ) + "x" + Helper::int2str( ncol ) + ", " + dclass + ")" );
    }
  sql.reset( stmt_dump_matrix_names ) ;
  return r;
}

blob MatDBase::make_blob( const Data::Vector<double> & x , int col )
{

  // if not symmetric, col == -1 
  // else is col number (0..ncol-1)

  double_t pbCol;

  const int upr = col == -1 ? x.size() : ( col + 1 ) ; 
    
  for (int r=0;r<upr;r++)
    pbCol.add_data( x[r] ); 
  
  std::string s;
  pbCol.SerializeToString(&s);
  return blob(s);
}


Data::Vector<double> MatDBase::read_blob( blob & b , int nrow )
{  
  // if matrix is symmetric, take nrow from'r'
  // otherwise, r==0, so just read what is here

  double_t pbCol;  
  pbCol.ParseFromString( b.get_string() );
  
  const int nrow_stored = pbCol.data_size();      
  const int nrow_make   = nrow == -1  ? nrow_stored : nrow ;
  
  Data::Vector<double> x( nrow_make );
  for (int r=0;r<nrow_stored;r++) 
    x[r] = pbCol.data(r);
  return x;

}


  
bool MatDBase::write( const Data::Matrix<double> & m , const std::string & label , bool symmetric )
{
  
  // main matrix

  std::string dclass = symmetric ? "SYMMETRIC" : "FULL" ;

  sql.bind_text( stmt_insert_matrix , ":name" , label ); 
  sql.bind_int( stmt_insert_matrix , ":nrow" , m.dim1() );
  sql.bind_int( stmt_insert_matrix , ":ncol" , m.dim2() );
  sql.bind_text( stmt_insert_matrix , ":dclass" , dclass );
  
  sql.step( stmt_insert_matrix );
  sql.reset( stmt_insert_matrix );
  
  uint64_t mat_id = sql.last_insert_rowid();
  
  // each columns
  for (int c=0;c<m.dim2();c++)
    {      
      blob data = make_blob( m.col(c) , symmetric ? c : -1 );      
      sql.bind_int64( stmt_insert_column , ":mat_id" , mat_id );
      sql.bind_int64( stmt_insert_column , ":col_id" , c );
      sql.bind_blob( stmt_insert_column , ":data" , data );  
      sql.step( stmt_insert_column );
      sql.reset( stmt_insert_column );
    }
  
  return true;
}
  

Data::Matrix<double> MatDBase::read( const std::string & label )
{
  
  sql.bind_text( stmt_lookup_matrix , ":name" , label );
  
  if ( sql.step( stmt_lookup_matrix ) )
    {
      uint64_t mat_id = sql.get_int64( stmt_lookup_matrix , 0 ) ;
      int nrow = sql.get_int( stmt_lookup_matrix , 1 );
      int ncol = sql.get_int( stmt_lookup_matrix , 2 );
      std::string dclass = sql.get_text( stmt_lookup_matrix , 3 );
      
      bool symmetric = dclass == "SYMMETRIC";

      sql.reset( stmt_lookup_matrix );
      

      sql.bind_int64( stmt_lookup_column , ":mat_id" , mat_id );
      
      // create this matrix
      Data::Matrix<double> m;
      for (int c=0;c<ncol;c++)
	{

	  if ( ! sql.step( stmt_lookup_column ) ) 
	    Helper::halt( "internal error in MatDBase" );
	  
	  int col_id = sql.get_int( stmt_lookup_column , 0 );
	  
	  blob tmp = sql.get_blob( stmt_lookup_column , 1 );

	  Data::Vector<double> T = read_blob( tmp , symmetric ? nrow  : -1 );

	  m.add_col( T );
	}
      
      sql.reset( stmt_lookup_column );
      
      // need to fill in 
      if ( symmetric ) 
	{
	  for (int c=0;c<m.dim2();c++)
	    for (int r=c+1;r<m.dim1();r++)
	      m(r,c) = m(c,r);
	}

      return m;


    }
  else
    {
      plog.warn( "could not find matrix" , label );
      sql.reset( stmt_lookup_matrix );
    }   
  
  return Data::Matrix<double>();
}


void MatDBase::wipe( const std::string & name )
{
  sql.query("DELETE ALL FROM matrices;");
}

