/*
	//
	// TMP test (1)
	// Test run Dijkstra
	//

	// for (int i=0;i<5;i++)
	//   {
	//     std::cout << "\n\n ----- SOURCE NODE " << nodes[i]->gene->getLabel() << "\n" ;
	//     paths_t dijkstra = Dijkstra( nodes[i] );
	//   }

	//
	// TMP test (2)
	//

	// Data::Matrix<double> D(4,4);
	// D(0,0) = 1;   D(0,1) = 0;   D(0,2) = 0;   D(0,3) = 0;
	// D(1,0) = 6;   D(1,1) = 0;   D(1,2) = 0;   D(1,3) = 0;
	// D(2,0) = 1;   D(2,1) = 6;   D(2,2) = 0;   D(2,3) = 0;
	// D(3,0) = 0;   D(3,1) = 0;   D(3,2) = 6;   D(3,3) = 2;

	//
	// Generate heat diffusion kernel matrix
	//

	// H=e^{-Lt}

	Data::Matrix<double> L = Laplacian();

	//  std::cout << L.print("L") << "\n";
	//  exit(0);

	L.inplace_multiply(-0.1); // t = 0.1

	Data::Matrix<double> H = Statistics::exponential(L);

	std::cout << H.print("H") << "\n";

	exit(0);

	//
	// EXAMPLE : generating a list of connected components
	//
	if (false) {
		EquivNodesList* pV = connected_components();

		// and iterate
		EquivNodesList::const_iterator connIt = pV->begin();
		int csz = pV->size();
		int ci = 0;
		while (connIt != pV->end()) {
			std::cout << "connected_component\t" << ++ci;
			const DisjointSet<const Node*>::EquivSet& cs = connIt->second;
			DisjointSet<const Node*>::EquivSet::const_iterator csIt = cs.begin();
			while (csIt != cs.end()) {
				std::cout << "\t" << (*csIt)->getGene()->getLabel();
				++csIt;
			}
			std::cout << "\n";
			++connIt;
		}
	}
*/

//
// Get Laplacian
//

//  Data::Matrix<double> L = Laplacian();

//  std::cout << "\n" << L.print( "L" )  << "\n";

//
// get Eigenvalues of L
//

// bool okay = true;
// Data::Vector<double> eigen = Statistics::eigenvalues( L , & okay );
// std::cout << "okay = " << okay << "\n";
// std::vector<double> x = eigen.extract();
// std::sort( x.begin() , x.end() );
// std::cout << "eigen values = \n" << Helper::print( x , 1,1 ) << "\n";

//  Data::Matrix<double> L2 = Normalized_Laplacian();

//  std::cout << "\n" << L2.print( "L" )  << "\n";

//
// get Eigenvalues of L
//

// okay = true;
// Data::Vector<double> eigen2 = Statistics::eigenvalues( L2 , & okay );
// std::cout << "okay = " << okay << "\n";
// std::sort( eigen2.data_pointer()->begin() , eigen2.data_pointer()->end() );
// std::cout << "eigen values = \n" << eigen2.print( "Eigenvalues" ) << "\n";
